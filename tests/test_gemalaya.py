import pytest
from pathlib import Path
from yarl import URL

from gemalaya.identities import IdentitiesManager


@pytest.fixture
def identsdir(tmpdir):
    return Path(tmpdir)


@pytest.fixture
def identscfg(tmpdir):
    return Path(tmpdir).joinpath('identities.yaml')


class TestGemalayaIdentities:
    @pytest.mark.parametrize('url', ['gemini://localhost'])
    def test_identities_create(self, identsdir, identscfg,
                               url):
        mgr = IdentitiesManager(identsdir, identscfg)
        assert len(mgr.list()) == 0

        mgr.create('john', commonName='test.org')
        mgr.create('anna', commonName='nope.org')
        assert 'john' in mgr.list()
        assert 'anna' in mgr.list()

        mgr.update('john', color='red')

        with pytest.raises(Exception):
            mgr.create('john', 'test.org')

        assert mgr.map(URL('gemini://localhost'), 'john')
        assert mgr.map(URL('gemini://bbs.geminispace.org/test'), 'john')
        assert mgr.mapRoot(
            URL('gemini://capsule.org/some/path'), 'anna',
            mapTitan=True
        )

        assert mgr.identityForUrl(url)[0] == 'john'
        assert mgr.identityForUrl(url + '/test')[0] == 'john'

        assert mgr.identityForUrl('gemini://bbs.geminispace.org')[0] is None
        assert mgr.identityForUrl(
            'gemini://bbs.geminispace.org/test')[0] == 'john'
        assert mgr.identityForUrl(
            'gemini://bbs.geminispace.org/test/subpath')[0] == 'john'

        assert mgr.identityForUrl(
            'gemini://bbs.geminispace.org:7999')[0] is None
        assert mgr.identityForUrl(
            'gemini://capsule.org')[0] == 'anna'

        assert mgr.identityForUrl('titan://capsule.org')[0] == 'anna'

        mgr.dump_config()
