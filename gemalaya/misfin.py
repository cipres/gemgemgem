import re
import socket

from yarl import URL

MA_REGEX = re.compile(
    r'([A-Z0-9._%+-]+)@([A-Z0-9.-]+(\.[A-Z]{2,63})?)',
    re.IGNORECASE
)


def verifyAddress(addr: str, connect: bool = True) -> bool:
    """
    Checks that a misfin address is valid, by connecting to the server's
    misfin service (can't do more than that, the protocol doesn't offer
    anything to do a mailbox check).
    """

    valid = False

    try:
        match = MA_REGEX.match(addr)
        assert match is not None

        if connect:
            host = match.group(2)

            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((host, 1958))

            s.shutdown(socket.SHUT_RDWR)
            s.close()

        valid = True
    except BaseException:
        valid = False

    return valid


def parseLink(link: str) -> dict:
    """
    Parse a misfin address link. The format should follow RFC 3986.
    If multiple addresses are specified, they must be separated by a comma.
    The optional query string contains the initial message body.

    misfin:user@domain.org?message
    misfin:user@domain.org,user2@domain2.org?message
    """

    addrs = []

    url = URL(link) if isinstance(link, str) else None

    if url is None or url.scheme != 'misfin' or url.path.startswith('/'):
        # Not a valid misfin: link
        return {
            'valid': False,
            'addrs': [],
            'message': None
        }

    for addr in url.path.lstrip().split(','):
        addrc = addr.strip()

        if MA_REGEX.match(addrc):
            addrs.append(addrc)

    return {
        'valid': True if len(addrs) > 0 or url.query_string else False,
        'addrs': addrs,
        'message': url.query_string
    }
