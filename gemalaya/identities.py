import json
from pathlib import Path
from datetime import datetime
from yarl import URL

from omegaconf import OmegaConf

from gemgemgem.x509 import x509SelfSignedGenerate


def rootify_url(url: URL):
    return url.with_path(
        '/').with_fragment(None).with_user(None).with_password(
            None).with_query(None)


class IdentitiesManager:
    def __init__(self, idents_path: Path,
                 identities_cfg_path: Path):
        self.identities_path = idents_path

        self.cfg_path = identities_cfg_path
        self.cfg_bkp_path = self.cfg_path.with_suffix(
            self.cfg_path.suffix + '.bkp'
        )

        self.config = self.load_config()

    def __save_config(self):
        OmegaConf.save(config=self.config, f=str(self.cfg_path))
        OmegaConf.save(config=self.config, f=str(self.cfg_bkp_path))

    def dump_config(self):
        print(json.dumps(OmegaConf.to_container(self.config), indent=4))

    def load_config(self):
        if self.cfg_path.is_file():
            with open(self.cfg_path, 'rt') as fd:
                cfg = OmegaConf.load(fd)
        else:
            cfg = OmegaConf.create({})

        if 'mappings' not in cfg:
            cfg.mappings = {}
        if 'identities' not in cfg:
            cfg.identities = {}

        return cfg

    def ident_certs_dirpath(self, name: str) -> Path:
        p = self.identities_path.joinpath(name)
        p.mkdir(parents=True, exist_ok=True)
        return p

    def list(self) -> list:
        return self.config.identities.keys()

    def map(self, url: URL, name: str) -> bool:
        if not url.scheme or url.scheme != 'gemini':
            return

        self.config.mappings[str(url)] = {
            'identity': name
        }
        self.__save_config()
        return True

    def mapRoot(self, url: URL, name: str,
                mapTitan: bool = True) -> bool:
        """
        Map the root of a gemini URL to an identity
        """

        urlr = url.with_path('/')

        if not urlr.scheme or urlr.scheme != 'gemini':
            return

        self.config.mappings[str(urlr)] = {
            'identity': name
        }

        if mapTitan:
            self.config.mappings[str(urlr.with_scheme('titan'))] = {
                'identity': name
            }

        self.__save_config()
        return True

    def unmap(self, url: URL, name: str):
        if str(url) in self.config.mappings:
            del self.config.mappings[str(url)]

            self.__save_config()
            return True

    def identityExists(self, name: str) -> bool:
        return name in self.config.identities

    def identityForUrl(self, urls: str) -> tuple:
        url = URL(urls)
        url_path = Path(url.path)

        for urlms, m in self.config.mappings.items():
            if 'identity' not in m:
                continue

            murl = URL(urlms)
            murl_path = Path(murl.path)

            if urlms == str(url):
                return m.identity, self.config.identities.get(m.identity)

            if url_path.is_relative_to(murl_path) and rootify_url(
                    murl) == rootify_url(url):
                return m.identity, self.config.identities.get(m.identity)

        return None, None

    def get(self, ident_name: str):
        return self.config.identities.get(ident_name)

    def create(self, ident_name: str,
               commonName: str = 'gemalaya.gitlab.io',
               displayName: str = None,
               orgName: str = None,
               monthsValid: int = 12 * 10,
               color: str = None) -> bool:
        idir = self.ident_certs_dirpath(ident_name)

        if ident_name in self.config.identities:
            raise ValueError(f'Identity {ident_name} already exists')

        if idir.is_dir() and len(list(idir.glob('*.crt'))) > 0:
            raise ValueError(f'Identity {ident_name} already exists')

        certp, keyp = x509SelfSignedGenerate(
            commonName,
            monthsValid=monthsValid,
            destDir=idir
        )

        self.__idstore(ident_name, certp, keyp,
                       commonName=commonName,
                       displayName=displayName,
                       color=color)

        self.__save_config()

        return True

    def __idstore(self, ident_name: str,
                  certp: Path,
                  keyp: Path,
                  commonName: str = None,
                  displayName: str = None,
                  color: str = None,
                  icon: str = None):
        now = datetime.now().isoformat()

        self.config.identities[ident_name] = {
            'certPath': str(certp),
            'keyPath': str(keyp),
            'commonName': commonName,
            'displayName': displayName if displayName else ident_name,
            'color': color,
            'icon': None,
            'dateCreated': now,
            'dateModified': now
        }

        self.__save_config()

    def _create_default(self, certp: Path, keyp: Path):
        self.__idstore('default', certp, keyp,
                       displayName='default',
                       color='green')

    def update(self, ident_name: str,
               color: str = None,
               icon: str = None,
               displayName: str = None) -> None:
        ident = self.config.identities.get(ident_name)
        if not ident:
            raise ValueError(f'Identity {ident_name} does not exist')

        if color:
            ident['color'] = color

        ident['dateModified'] = datetime.now().isoformat()
        self.__save_config()
