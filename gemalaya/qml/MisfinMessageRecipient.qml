import QtQuick 2.14
import QtQuick.Controls 2.14

RowLayout {
  property alias address: recipient.text
  property string defaultBgColor: 'white'

  function setAddress(addr) {
    recipient.text = addr
  }

  Text {
    Layout.maximumWidth: parent.width * 0.4
    text: qsTr('Recipient address')
    font.pointSize: 12
    font.bold: true
    color: '#8B4513'
  }
  TextField {
    id: recipient
    Layout.fillWidth: true
    Layout.minimumWidth: parent.width * 0.25
    font.pointSize: 14
    maximumLength: 128
    background: Rectangle {
      id: recipientBg
      color: defaultBgColor
      border.width: 1
      border.color: 'black'
    }
    onTextChanged: recipientBg.color = defaultBgColor
  }

  function onContactSelected(addr) {
    setAddress(addr)

    checkAddr.click()
  }

  Button {
    font.pointSize: 14
    text: qsTr('Contacts')
    icon.source: Conf.themeRsc('addressbook-search.png')
    icon.width: 16
    icon.height: 16
    onClicked: {
      var component = Qt.createComponent('MisfinContactSelector.qml')
      var win = component.createObject(this, {
        width: 320,
        height: 150
      })
      win.selected.connect(onContactSelected)
      win.open()
    }
  }

  Button {
    id: checkAddr
    font.pointSize: 14
    text: qsTr('Verify')
    onClicked: {
      recipientBg.color = 'white'
      recipientBg.border.width = 1

      var valid = gemalaya.verifyMisfinAddress(recipient.text, true)

      if (valid === true) {
        recipientBg.color = 'lightgreen'
        recipientBg.border.width = 2
      } else {
        recipientBg.color = 'indianred'
      }
    }
  }
}
