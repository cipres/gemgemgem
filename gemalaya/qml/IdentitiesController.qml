import QtQuick 2.2
import QtQuick.Controls 2.15

ToolButton {
  id: ictrl
  icon.source: Conf.themeRsc('identity.png')

  icon.color: iconcolor
  hoverEnabled: true

  ToolTip {
    id: tooltip
    text: identAction && identAction.identity ?
          identAction.displayNameFormat() : qsTr('No identity selected')
    visible: hovered
    font.family: "DejaVu sans"
    font.pointSize: 16
    timeout: 1500
  }

  SequentialAnimation {
    id: anim
    ScaleAnimator {
      target: ictrl
      from: 0.5
      to: 1.5
      duration: 800
    }
    ScaleAnimator {
      target: ictrl
      from: 1.5
      to: 1
      duration: 500
    }
  }

  property string incognitoColor: Conf.theme.identity.incognito.color

  property string iconcolor: incognitoColor
  property Item addrController
  property IdentitySwitchAction identAction

  property bool automap: Conf.ui.identities.autoMapUUrlToCurrent

  property bool incognito: false

  property string toolTipNoIdentity: qsTr('Not using any identity now (incognito)')

  signal reset()
  signal browsingUnmappedUrl(url uurl)
  signal switched(Action action)
  signal usingIdentity(string iname)

  signal needPageReload(Action action)

  function shortcutsHelp() {
    var tooltip = qsTr('Identity shortcuts') + '\n\n'

    for (let idx=0; idx < resources.length; idx++) {
      let action = resources[idx]

      if (!action.objectName.startsWith('IdentitySwitchAction'))
        continue

      tooltip += `Press ${action.shortcut} for identity: ${action.iname}\n`
    }

    tooltip += qsTr(`Press ${Conf.identityShortcuts.enterIncognito} to enter incognito mode\n`)
    tooltip += qsTr(`Press ${Conf.identityShortcuts.leaveIncognito} to leave incognito mode\n`)

    return tooltip
  }

  onHoveredChanged: {
    if (hovered && identAction == null)
      tooltip.show(shortcutsHelp(), 3500)
  }

  onBrowsingUnmappedUrl: (uurl) => {
    if (identAction != null && automap) {
      identities.mapRoot(uurl, identAction.iname)
    } else {
      reset()
    }
  }

  onUsingIdentity: (iname) => {
    if (incognito) {
      return
    }

    for (let idx=0; idx < resources.length; idx++) {
      let action = resources[idx]

      if (action.iname == iname) {
        action.act()
        switched(action)
        break
      }
    }
  }

  onNeedPageReload: (action) => {
    addrController.requestCurrent()
  }

  onReset: {
    identAction = null
    iconcolor = incognitoColor
  }

  onIdentActionChanged: {
    if (identAction != null) {
      anim.running = true
      tooltip.show(tooltip.text, 2500)
    }
  }

  onSwitched: (action) => {
    iconcolor = action.icolor

    if (identAction != action) {
      identAction = action
      identAction.load()
    }
  }

  Action {
    shortcut: Conf.identityShortcuts.enterIncognito
    enabled: ictrl.visible && !incognito
    onTriggered: {
      incognito = true
      reset()
      needPageReload(identAction)
    }
  }
  Action {
    shortcut: Conf.identityShortcuts.leaveIncognito
    enabled: ictrl.visible && incognito
    onTriggered: {
      incognito = false
      needPageReload(identAction)
    }
  }

  IdentitySwitchAction {
    shortcut: Conf.ui.identityShortcuts.identitySwitch1
    iname: 'default'
    icolor: 'green'
    addrc: addrController
    ctrler: ictrl
    enabled: ictrl.visible
  }
  IdentitySwitchAction {
    shortcut: Conf.ui.identityShortcuts.identitySwitch2
    iname: 'alpha'
    icolor: 'blue'
    addrc: addrController
    ctrler: ictrl
    enabled: ictrl.visible
  }
  IdentitySwitchAction {
    shortcut: Conf.ui.identityShortcuts.identitySwitch3
    iname: 'beta'
    icolor: 'yellow'
    addrc: addrController
    ctrler: ictrl
    enabled: ictrl.visible
  }
  IdentitySwitchAction {
    shortcut: Conf.ui.identityShortcuts.identitySwitch4
    iname: 'gamma'
    icolor: 'red'
    addrc: addrController
    ctrler: ictrl
    enabled: ictrl.visible
  }
  IdentitySwitchAction {
    shortcut: Conf.ui.identityShortcuts.identitySwitch5
    iname: 'delta'
    icolor: 'orange'
    addrc: addrController
    ctrler: ictrl
    enabled: ictrl.visible
  }
  IdentitySwitchAction {
    shortcut: Conf.ui.identityShortcuts.identitySwitch6
    iname: 'epsilon'
    icolor: '#fa8072'
    addrc: addrController
    ctrler: ictrl
    enabled: ictrl.visible
  }
  IdentitySwitchAction {
    shortcut: Conf.ui.identityShortcuts.identitySwitch7
    iname: 'zeta'
    icolor: 'magenta'
    addrc: addrController
    ctrler: ictrl
    enabled: ictrl.visible
  }
  IdentitySwitchAction {
    shortcut: Conf.ui.identityShortcuts.identitySwitch8
    iname: 'eta'
    icolor: 'skyblue'
    addrc: addrController
    ctrler: ictrl
    enabled: ictrl.visible
  }
  IdentitySwitchAction {
    shortcut: Conf.ui.identityShortcuts.identitySwitch9
    iname: 'theta'
    icolor: 'turquoise'
    addrc: addrController
    ctrler: ictrl
    enabled: ictrl.visible
  }
  IdentitySwitchAction {
    shortcut: Conf.ui.identityShortcuts.identitySwitch10
    iname: 'iota'
    icolor: 'aqua'
    addrc: addrController
    ctrler: ictrl
    enabled: ictrl.visible
  }

  function listIdentities() {
    console.log(JSON.stringify(identities.list()))
  }
}
