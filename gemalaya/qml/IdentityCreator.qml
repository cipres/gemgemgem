import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Dialogs

Dialog {
  id: dialog

  property string cn
  property string name

  property string identityName
  property string initColor

  signal approved(string commonName, string displayName, color identityColor,
                  int vMonths)

  ColumnLayout {
    id: root

    RowLayout {
      Layout.margins: 20
      Text {
        text: qsTr('Create a gemini identity')
        Layout.fillWidth: true
        horizontalAlignment: Text.AlignHCenter
        font.pointSize: 18
        font.bold: true
      }
    }

    RowLayout {
      Layout.margins: 20
      Text {
        text: qsTr('Common name')
        font.pointSize: 14
      }
      TextField {
        id: commonName
        text: cn
        Layout.fillWidth: true
        font.pointSize: 16
        maximumLength: 64
        focus: true
        validator: RegularExpressionValidator {
          regularExpression: /^([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,}$/
        }
      }
    }
    RowLayout {
      Layout.margins: 20
      Text {
        text: qsTr('Display name')
        font.pointSize: 14
      }
      TextField {
        id: displayName
        text: name
        font.pointSize: 16
        Layout.fillWidth: true
        maximumLength: 32
        validator: RegularExpressionValidator {
          regularExpression: /^[A-Za-z0-9\s]+$/
        }
      }
    }

    RowLayout {
      Layout.margins: 10
      Text {
        text: qsTr('Certificate validity, in months')
        font.pointSize: 14
      }
      SpinBox {
        font.pointSize: 16
        id: validityMonths
        value: 60
        stepSize: 1
        from: 1
        to: 12 * 100
      }
    }

    RowLayout {
      ColorDialog {
        id: colorDialog
        selectedColor: identityColor.color
        onAccepted: {
          identityColor.color = selectedColor
        }
      }

      Text {
        text: qsTr('Identity color')
        font.pointSize: 14
        Layout.fillWidth: true
        horizontalAlignment: Text.AlignHCenter
      }
      Rectangle {
        id: identityColor
        color: initColor
        radius: 16
        Layout.preferredWidth: 128
        Layout.preferredHeight: 128
        border.width: mouse.hovered ? 2 : 1
        border.color: 'black'

        HoverHandler {
          id: mouse
          acceptedDevices: PointerDevice.Mouse | PointerDevice.TouchPad
        }

        MouseArea {
          anchors.fill: parent
          onClicked: {
            colorDialog.open()
          }
        }
      }
    }

    RowLayout {
      Layout.margins: 20
      Button {
        text: qsTr('Cancel')
        font.pointSize: 12
        Layout.fillWidth: true
        onClicked: {
          dialog.close()
          dialog.destroy()
        }
      }

      Button {
        text: qsTr('Create (Ctrl+Return)')
        Layout.fillWidth: true
        font.pointSize: 14
        font.bold: true

        action: Action {
          shortcut: 'Ctrl+Return'
          onTriggered: {
            if (commonName.text.length == 0) {
              commonName.forceActiveFocus()
              return
            }

            if (displayName.text.length == 0) {
              displayName.forceActiveFocus()
              return
            }

            approved(commonName.text, displayName.text, identityColor.color,
                     validityMonths.value)

            dialog.close()
            dialog.destroy()
          }
        }
      }
    }
  }
}
