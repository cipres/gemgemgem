import QtCore
import QtQuick
import QtQuick.Controls
import QtQuick.Dialogs

FileDialog {
  id: fileDialog

  property url titanUrl
  signal selected(string path)

  currentFolder: StandardPaths.standardLocations(StandardPaths.HomeLocation)[0]
  visible: true
  onAccepted: {
    var path = selectedFile.toString().replace('file://', '')

    fileDialog.done(0)

    selected(path)
  }

  onRejected: {
    close()
  }
}
