import QtQuick 2.15
import QtQuick.Controls 2.15

Action {
  id: action

  objectName: 'IdentitySwitchAction'

  property string icolor
  property string iname
  property Item addrc
  property Item ctrler

  property var identity

  signal switched()
  signal act()

  function displayNameFormat() {
    return qsTr(`Identity: ${identity.displayName}\nCommon name: ${identity.commonName ? identity.commonName : 'unknown'}`)
  }

  onTriggered: {
    var curl = addrc.urlObject()

    if (!identities.identityExists(iname)) {
      try {
        var dialog = Qt.createComponent('IdentityCreator.qml').createObject(
          this, {
            cn: curl != null ? curl.hostname : '',
            initColor: icolor,
            x: -400,
            y: ctrler.height * 2
          }
        )

        dialog.approved.connect(function(cname, displayName, identColor, vMonths) {
          identities.create(iname, {
            commonName: cname,
            displayName: displayName,
            monthsValid: vMonths,
            color: identColor
          })

          action.act()
          ictrl.needPageReload(action)
        })

        dialog.open()
      } catch(e) {
        console.log('Could not create identity: ' + e)
        return
      }
    } else {
      act()
      ictrl.needPageReload(action)
    }
  }

  function load() {
    identity = identities.get(iname)

    if (identity) {
      icolor = identity.color
    }
  }

  onAct: {
    var curl

    load()

    curl = addrc.urlObject()

    if (curl != null && curl.protocol == 'gemini:') {
      identities.mapRoot(addrc.url, iname)
    }

    ctrler.switched(this)
  }
}
