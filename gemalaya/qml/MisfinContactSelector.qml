import QtQuick 2.14
import QtQuick.Controls 2.14

Popup {
  id: popup
  closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
  background: Rectangle {
    color: '#778899'
    border.width: 1
    border.color: 'black'
  }

  x: 0
  y: 0

  signal selected(string address)

  contentItem: MisfinContactsView {
    anchors.fill: parent
    onAddrClicked: function(address) {
      selected(address)
      popup.close()
    }
  }
}
