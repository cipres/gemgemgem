import QtQuick 2.2
import QtQuick.Controls 2.4

ListView {
  id: view
  focus: true
  clip: true
  model: misfinContactsModel
  spacing: 4

  signal addrClicked(string address)

  ScrollBar.vertical: ScrollBar {
    parent: view
    width: 20
    policy: ScrollBar.AlwaysOn
  }

  delegate: Text {
    font.family: "DejaVu sans"
    font.pointSize: 12
    text: display
    MouseArea{
      anchors.fill: parent
      onClicked: {
        addrClicked(text)
      }
    }
  }
}
