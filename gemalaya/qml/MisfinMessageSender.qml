import QtQuick 2.14
import QtQuick.Controls 2.14

Popup {
  id: popup
  closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
  background: Rectangle {
    color: '#778899'
    border.width: 1
    border.color: 'black'
  }

  x: ~~(Overlay.overlay.x + 64)
  y: ~~(Overlay.overlay.y + 64)
  width: ~~(0.7 * Overlay.overlay.width)
  height: ~~(0.7 * Overlay.overlay.height)

  /* Maximum content-length for Misfin(C) messages */
  property int maxContentLength: 16384

  property int maxRecipients: 5

  property var recipients: []
  property string initialMessage
  property alias message: message

  Component.onCompleted: {
    if (recipients.length > 0) {
      /* Set the passed recipients in the text fields */
      for (let idx = 0; idx < recipients.length; idx++) {
        recpcontrols.add(recipients[idx])
      }
    } else {
      recpcontrols.add(null)
    }
  }

  Misfin {
    id: client
    onSent: function(returncode) {
      recpcontrols.addresses().forEach(function(addr) {
        misfinContactsModel.addContact(addr, '')
      })

      popup.close()
      popup.destroy()
    }
    onSendError: function(errorMessage) {
      sendError.informativeText = errorMessage
      sendError.open()
    }
  }

  MessageDialog {
    id: invalidRecipientsError
    title: qsTr("Invalid recipients")
    text: qsTr("Invalid recipients: please specify at least one valid recipient address")
    buttons: MessageDialog.Ok
  }

  MessageDialog {
    id: sendError
    title: qsTr("Error")
    text: qsTr("Send error")
    buttons: MessageDialog.Ok
  }
  MessageDialog {
    id: sendSuccess
    title: qsTr("Success")
    text: qsTr("The message was sent successfully!")
    buttons: MessageDialog.Ok
  }

  function validMisfinAddr(addr) {
    return gemalaya.verifyMisfinAddress(addr, false)
  }

  contentItem: ColumnLayout {
    id: root

    RowLayout {
      Text {
        text: qsTr("Misfin: compose a message")
        font.pointSize: 18
        font.bold: true
        horizontalAlignment: Text.AlignHCenter
        Layout.fillWidth: true
      }
    }

    RowLayout {
      Item {
        Layout.fillWidth: true
      }

      Button {
        text: qsTr("Add a recipient")
        enabled: recpcontrols.children.length < maxRecipients
        font.pointSize: 14
        onClicked: {
          recpcontrols.add(null)
        }
      }
      Button {
        text: qsTr("Remove last recipient")
        enabled: recpcontrols.children.length > 1
        font.pointSize: 14
        onClicked: {
          recpcontrols.children[recpcontrols.children.length - 1].destroy()
        }
      }
    }

    ColumnLayout {
      id: recpcontrols

      function add(addr) {
        Qt.createComponent('MisfinMessageRecipient.qml').createObject(this, {
          address: addr
        })
      }

      function addresses() {
        var vrecipients = []

        for (let idx = 0; idx < children.length; idx++) {
          let addr = children[idx].address

          if (validMisfinAddr(addr)) {
            vrecipients.push(addr)
          }
        }

        return vrecipients
      }
    }

    Flickable {
      clip: true
      contentWidth: message.paintedWidth
      contentHeight: message.paintedHeight
      Layout.fillWidth: true
      Layout.fillHeight: true

      ScrollBar.vertical: ScrollBar {
        policy: ScrollBar.AlwaysOn
        width: 15
      }

      TextArea.flickable: TextArea {
        id: message
        text: initialMessage
        font.pointSize: 18
        font.family: 'DejaVu sans'
        wrapMode: TextEdit.WordWrap
      }
    }

    RowLayout {
      Text {
        id: charCounter
        font.pointSize: 14
        font.bold: true
        color: message.length > maxContentLength ? 'red' : 'black'
        text: message.length.toString() + '/' + maxContentLength.toString()
      }

      Button {
        text: 'Cancel'
        font.pointSize: 16
        Layout.fillWidth: true
        onClicked: {
          popup.close()
        }
      }

      Button {
        id: send
        text: 'Send (Ctrl+s)'
        enabled: message.text.length > 0 && validMisfinAddr(recpcontrols.children[0].address)
        font.bold: enabled
        font.pointSize: 20
        Layout.fillWidth: true
        action: Action {
          shortcut: 'Ctrl+s'
        }
        onClicked: {
          var vrecipients = recpcontrols.addresses()

          if (vrecipients.length === 0) {
            invalidRecipientsError.open()
            return
          }

          client.send(
            client.defaultIdentityPath(),
            vrecipients,
            message.text
          )
        }
      }
    }
  }
}
