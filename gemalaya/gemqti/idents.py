from yarl import URL

from PySide6.QtCore import QObject
from PySide6.QtCore import Slot
from PySide6.QtWidgets import QApplication

from pathlib import Path


class IdentitiesInterface(QObject):
    def __init__(self,
                 cfg_dir_path: Path,
                 identities_cfg_path: Path,
                 config,
                 parent=None):
        super().__init__(parent)

        self.app = QApplication.instance()
        self.identities_cfg_path = identities_cfg_path

    @Slot(str, dict, result=bool)
    def create(self, name: str, opts: dict):
        color_rgb = opts['color'].name() if 'color' in opts else None

        return self.app.identities.create(
            name,
            commonName=opts.get('commonName'),
            displayName=opts.get('displayName'),
            orgName=opts.get('orgName'),
            monthsValid=opts.get('monthsValid', 12),
            color=color_rgb
        )

    @Slot(str, result=dict)
    def get(self, name: str):
        return dict(self.app.identities.get(name))

    @Slot(str, dict, result=bool)
    def update(self, name: str, meta: dict):
        return self.app.identities.update(name, **meta)

    @Slot(str, result=bool)
    def identityExists(self, name: str):
        return self.app.identities.identityExists(name)

    @Slot(str, str, result=bool)
    def map(self, url: str, name: str):
        return self.app.identities.map(URL(url), name)

    @Slot(str, str, result=bool)
    def mapRoot(self, url: str, name: str):
        return self.app.identities.mapRoot(URL(url), name)

    @Slot(str, result=bool)
    def mapped(self, url: str):
        return self.app.identities.mapped(URL(url))

    @Slot(str, str, result=bool)
    def unmap(self, url: str, name: str):
        return self.app.identities.unmap(url, name)

    @Slot(str, result=dict)
    def identityForUrl(self, url: str):
        ident_name, ident = self.app.identities.identityForUrl(url)

        if ident_name:
            resp = {
                'name': ident_name
            }
            resp.update(ident)
            return resp
        else:
            return {
                'name': ''
            }

    @Slot(result=list)
    def list(self):
        return self.app.identities.list()
